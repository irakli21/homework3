package com.example.Iraklidav3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class FinalActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final)

        findViewById<TextView>(R.id.textView1).text = intent.extras?.getString("name", "jora")
        findViewById<TextView>(R.id.textView2).text = intent.extras?.getInt("age", 20).toString()
        findViewById<TextView>(R.id.textView3).text = intent.extras?.getString("leqtori", "kacita")

    }
}