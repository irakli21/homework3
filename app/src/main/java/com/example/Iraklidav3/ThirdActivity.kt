package com.example.Iraklidav3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class ThirdActivity : AppCompatActivity() {

    private lateinit var leqtoritext: EditText
    private lateinit var buttonnext3: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        leqtoritext = findViewById(R.id.leqtoritext)
        buttonnext3 = findViewById(R.id.buttonnext3)

        var name = ""
        var age: Int

        if (intent.extras != null)
        {
            name = intent.extras?.getString("name").toString()
            age = intent.extras?.getInt("age").toString().toInt()
        }

        buttonnext3.setOnClickListener {

            var leqtori = leqtoritext.text.toString()

            val intent = Intent(this,FinalActivity::class.java)
            intent.putExtra("leqtori",leqtori)
            startActivity(intent)

        }

    }
}