package com.example.Iraklidav3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class SecondActivity : AppCompatActivity() {

    private lateinit var agetext: EditText
    private lateinit var buttonnext2: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        agetext = findViewById(R.id.agetext)
        buttonnext2 = findViewById(R.id.buttonnext2)

        var name = ""

        if (intent.extras != null)
        {
            name = intent.extras?.getString("name","").toString()
        }


        buttonnext2.setOnClickListener {

            val age = agetext.text.toString().toInt()

            val intent = Intent(this,ThirdActivity::class.java)
            intent.putExtra("age",age)
            intent.putExtra("name",name)
            startActivity(intent)

        }

    }
}