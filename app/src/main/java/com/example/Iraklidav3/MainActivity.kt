package com.example.Iraklidav3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var nametext : EditText
    private lateinit var buttonnext1: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nametext = findViewById(R.id.nametext)
        buttonnext1 = findViewById(R.id.buttonnext1)

        buttonnext1.setOnClickListener {

            val name = nametext.text.toString()

            val intent = Intent(this,SecondActivity::class.java)
            intent.putExtra("name",name)
            startActivity(intent)

        }

    }
}